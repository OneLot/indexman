# Indexman

```
Indexman gets the composition for the SP500 on batch when the market closes.
The weighting in % terms is saved in an msqlite db and is available in a GET request.

Runs on django.

Uses the composition from wikipedia, gets stock prices and outstanding shares on yahoo. 
```
