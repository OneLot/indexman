"""
indexman URL Configuration
For more information please visit
https://www.django-rest-framework.org/
https://www.djangoproject.com/
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='IndexManAPI')
urlpatterns = [
    path('', include('index.urls')),
    path('swagger/', schema_view),
    path('admin/', admin.site.urls),
]
urlpatterns += [
    path('api-auth/', include('rest_framework.urls')),
]
