from django.db import models
from pygments.lexers import get_all_lexers
from pygments.styles import get_all_styles

LEXERS = [item for item in get_all_lexers() if item[1]]
LANGUAGE_CHOICES = sorted([(item[1][0], item[0]) for item in LEXERS])
STYLE_CHOICES = sorted((item, item) for item in get_all_styles())


def pkgen():
    from base64 import b32encode
    from hashlib import sha1
    from random import random
    rude = ('lol',)
    bad_pk = True
    while bad_pk:
        pk = b32encode(sha1(str(random())).digest()).lower()[:6]
        bad_pk = False
        for rw in rude:
            if pk.find(rw) >= 0: bad_pk = True
    return pk


class IndexComponent(models.Model):
    name = models.CharField(blank=False, max_length=50)
    as_of = models.DateField(blank=False)
    ticker = models.CharField(blank=False, max_length=7, primary_key=True, default=pkgen)
    free_float = models.DecimalField(max_digits=16, decimal_places=2)
    listed_price = models.DecimalField(max_digits=10, decimal_places=2)
    weight = models.DecimalField(max_digits=17, decimal_places=15)

    class Meta:
        ordering = ('-weight',)


class ComponentId(models.Model):
    ticker = models.CharField(blank=False, max_length=7, primary_key=True, default=pkgen)
    pair_id = models.PositiveIntegerField()

    class Meta:
        ordering = ('ticker',)
