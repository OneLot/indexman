from index.models import IndexComponent, ComponentId
from django.contrib.auth.models import User
from rest_framework.response import Response
from index.serializers import IndexComponentSerializer, UserSerializer, ComponentIdSerializer
from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Dummy API')


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    This viewset automatically provides 'list' and 'detail' actions
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class IndexComponentViewSet(viewsets.ViewSet):
    queryset = IndexComponent.objects.all()

    def list(self, request):
        index_components = IndexComponent.objects.all()
        serializer = IndexComponentSerializer(index_components, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        index_components = IndexComponent.objects.all()
        compo = get_object_or_404(index_components, pk=pk)
        serializer = IndexComponentSerializer(compo, context={'request': request})
        return Response(serializer.data)


class ComponentViewSet(viewsets.ViewSet):
    queryset = ComponentId.objects.all()

    def list(self, request):
        component_ids = ComponentId.objects.all()
        serializer = ComponentIdSerializer(component_ids, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        component_ids = ComponentId.objects.all()
        compo_id = get_object_or_404(component_ids, pk=pk)
        serializer = ComponentIdSerializer(compo_id, context={'request': request})
        return Response(serializer.data)
