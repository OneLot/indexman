from django.urls import path, include
from rest_framework.routers import DefaultRouter
from index import views

router = DefaultRouter()
router.register(r'indexcompo', views.IndexComponentViewSet)
router.register(r'transco', views.ComponentViewSet)

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
]
