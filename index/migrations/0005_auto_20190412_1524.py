# Generated by Django 2.2 on 2019-04-12 15:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0004_auto_20190407_1355'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='indexcomponent',
            options={'ordering': ('-weight',)},
        ),
        migrations.AlterField(
            model_name='indexcomponent',
            name='as_of',
            field=models.DateField(),
        ),
        migrations.AlterField(
            model_name='indexcomponent',
            name='weight',
            field=models.DecimalField(decimal_places=15, max_digits=17),
        ),
    ]
