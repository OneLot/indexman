from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('index', '0008_add_prev_conflict_id'),
    ]
    operations = [
        migrations.RunSQL(
            """
            UPDATE `index_componentid`
            SET (pair_id) = 261 
            WHERE ticker = 'DE';
            
            UPDATE `index_componentid`
            SET (pair_id) = 6358 
            WHERE ticker = 'FITB';
            
            UPDATE `index_componentid`
            SET (pair_id) = 7937 
            WHERE ticker = 'COP';
            
            UPDATE `index_componentid`
            SET (pair_id) = 7938 
            WHERE ticker = 'CL';
            """
        )
    ]

