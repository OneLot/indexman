from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('index', '0007_init_componentid'),
    ]
    operations = [
        migrations.RunSQL(
            """
            INSERT INTO `index_componentid` (ticker,pair_id) 
            VALUES 
                ('BAC',243),
                ('MA',7864),
                ('BA',238),
                ('C',241),
                ('IBM',8082),
                ('CB',8175),
                ('MS',8056),
                ('CI',7889),
                ('MAR',8136),
                ('FAST',6409),
                ('AME',39219),
                ('AAP',32509),
                ('CBOE',39325),
                ('MAC',32359);     
            """
        )
    ]

