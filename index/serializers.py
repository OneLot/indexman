from typing import Any

from rest_framework import serializers
from index.models import IndexComponent, ComponentId
from django.contrib.auth.models import User


class IndexComponentSerializer(serializers.ModelSerializer):
    class Meta:
        model = IndexComponent
        # list_serializer_class = IndexComponentListSerializer
        fields = (
            'name',
            'as_of',
            'ticker',
            'free_float',
            'listed_price',
            'weight',
        )


class ComponentIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComponentId
        fields = (
            'ticker',
            'pair_id'
        )


class UserSerializer(serializers.HyperlinkedModelSerializer):
    snippets = serializers.HyperlinkedRelatedField(many=True,
                                                   view_name='snippet-detail',
                                                   read_only=True)

    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'index')
