from django.core.management.base import BaseCommand, CommandError
from functional import seq
import datetime
from composition import Client, Index, Stock
from typing import List
from django.shortcuts import get_object_or_404

from index.models import IndexComponent
from index.serializers import IndexComponentSerializer
import json


class Command(BaseCommand):
    args = ''
    help = 'Persists new compo data hidden from end points'

    def handle(self, *args, **options):
        client = Client()
        spx_500: Index = client.get_index(name="sp500")
        components: List[Stock] = spx_500.components
        components_list: List[dict] = (seq(components)
                                       .filter(lambda stock: stock.free_float is not None)
                                       .map(lambda stock: stock.to_dict())).to_list()

        if len(components_list) is 0:
            return
        IndexComponent.objects.all().delete()

        for obj in components_list:
            to_persist = IndexComponentSerializer(data=obj)
            can_persist = to_persist.is_valid()
            if can_persist:
                to_persist.create(validated_data=to_persist.data)
            else:
                print(vars(to_persist.errors))
        print('done updating')
